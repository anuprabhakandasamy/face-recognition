# Face recognition

Objective :
The intent of the challenge is to verify the identity of a person by matching their selfie
image with the image that is displayed in their ID document.

Description :
When the user runs the application, the camera should be turned on and it should
capture the live image of the user using the front camera in the system.
